package routers

import (
	"test5/controllers"
	"github.com/astaxie/beego"
)

func init() {
    beego.Router("/", &controllers.ConfiguratorController{})
}

package test

import (
	_ "test5/routers"
	"net/http"
	"net/http/httptest"
	"testing"
	"github.com/astaxie/beego"
	"bytes"
	"encoding/json"
	"test5/models"
	. "github.com/smartystreets/goconvey/convey"
)

func init() {
	//_, file, _, _ := runtime.Caller(1)
	//apppath, _ := filepath.Abs(filepath.Dir(filepath.Join(file, ".."+string(filepath.Separator))))
	//apppath, _ := filepath.Abs(filepath.Dir(os.Args[0]))
	beego.TestBeegoInit("/go/src/test5")
}

func TestPost(t *testing.T) {

	values := models.Configurator{
		Type: "Develop.mr_robot",
		Data: "Database.processing",
	}
	postData, _ := json.Marshal(values)

	Convey("test json", t, func() {
		r, _ := http.NewRequest("POST", "/", bytes.NewBuffer(postData))
		r.Header.Set("Content-Type", "application/json")
		w := httptest.NewRecorder()
		beego.BeeApp.Handlers.ServeHTTP(w, r)
		So(w.Body.String(), ShouldContainSubstring, "mr_robot")
	})
}


package models

type Configurator struct {
	Id    int    `orm:"pk" json:"-"`
	Type  string `json:"Type"`
	Data  string `json:"Data"`
	Value string `orm:"type(text)" json:"Value,omitempty"`
}

func (u *Configurator) TableName() string {
	return "configurator_configs"
}

func (u *Configurator) TableIndex() [][]string {
	return [][]string{
		[]string{"Type", "Data"},
	}
}

package services

import (
	"test5/models"
	"github.com/astaxie/beego/orm"
	"github.com/astaxie/beego"
)

func LoadConfig(data *models.Configurator) error {
	o := orm.NewOrm()
	if err := o.Read(data, "Type", "Data"); err != nil {
		if err == orm.ErrNoRows {
			return nil
		} else {
			// 500
			beego.Error("Internal error", err)
			return err
		}
	}

	return nil
}

package services

import (
	"github.com/astaxie/beego/logs"
)

func InitializeLogs() {
	log := logs.NewLogger()
	log.SetLogger(logs.AdapterConsole)
}

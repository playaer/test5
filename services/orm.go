package services

import (
	"github.com/astaxie/beego/orm"
	"test5/models"
	"fmt"
	"os"
	_ "github.com/lib/pq"
	"github.com/astaxie/beego"
)

func InitializeORM() {
	source := os.Getenv("ORM_SOURCE")
	if source == "" {
		beego.Critical("need source!")
		fmt.Println(`need source!`)
		os.Exit(2)
	}

	orm.RegisterDriver("postgres", orm.DRPostgres)
	orm.RegisterModel(new(models.Configurator))
	orm.RegisterDataBase("default", "postgres", source, 20)
}

package main

import (
	_ "test5/routers"
	"github.com/astaxie/beego"
	"test5/services"
)

func init() {
	services.InitializeLogs()
	services.InitializeORM()
}

func main() {
	beego.Run()
}


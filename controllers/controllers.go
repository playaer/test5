package controllers

import (
	"github.com/astaxie/beego"
	"test5/models"
	"encoding/json"
	"test5/services"
)

type ConfiguratorController struct {
	beego.Controller
}

func (this *ConfiguratorController) Post() {
	respData := map[string]interface{}{}
	this.Data["json"] = respData
	defer this.ServeJSON()

	var ob models.Configurator
	if err := json.Unmarshal(this.Ctx.Input.RequestBody, &ob); err != nil {
		beego.Debug("Invalid request", err, this.Ctx.Input.RequestBody)
		this.Abort("500")
		return
	}
	if ob.Type == "" || ob.Data == "" {
		beego.Debug("Invalid request", "empty Type or Data")
		this.Abort("500")
		return
	}
	beego.Debug("Request data", ob)

	if err := services.LoadConfig(&ob); err != nil {
		return
	}

	if err := json.Unmarshal([]byte(ob.Value), &respData); err != nil {
		beego.Error("Internal error: bad data in db", err)
		return
	}
	beego.Debug("Responce data", respData)

	this.Data["json"] = respData
}

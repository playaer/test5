package main

import (
	"github.com/astaxie/beego/migration"
)

// DO NOT MODIFY
type Fixtures_20180119_113351 struct {
	migration.Migration
}

// DO NOT MODIFY
func init() {
	m := &Fixtures_20180119_113351{}
	m.Created = "20180119_113351"

	migration.Register("Fixtures_20180119_113351", m)
}

// Run the migrations
func (m *Fixtures_20180119_113351) Up() {
	m.SQL(`
	INSERT INTO "configurator_configs" (type, data, value)
	VALUES ('Develop.mr_robot', 'Database.processing', '
	{"host":"localhost","port":"5432","database":"devdb","user":"mr_robot","password":"secret","schema":"public"}');
	`);
	m.SQL(`
	INSERT INTO "configurator_configs" (type, data, value)
	VALUES ('Test.vpn', 'Rabbit.log', '{"host":"10.0.5.42","port":"5671","virtualhost":"/","user":"guest","password":"guest"}');
	`);

}

// Reverse the migrations
func (m *Fixtures_20180119_113351) Down() {
	m.SQL(`DELETE FROM "configurator_configs" WHERE type="Develop.mr_robot" AND data="Database.processing"`);
	m.SQL(`DELETE FROM "configurator_configs" WHERE type="Test.vpn" AND data="Rabbit.log"`);

}

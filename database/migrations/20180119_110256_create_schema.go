package main

import (
	"github.com/astaxie/beego/migration"
	_ "github.com/lib/pq"
	_ "github.com/go-sql-driver/mysql"
)

// DO NOT MODIFY
type CreateSchema_20180119_110256 struct {
	migration.Migration
}

// DO NOT MODIFY
func init() {
	m := &CreateSchema_20180119_110256{}
	m.Created = "20180119_110256"

	migration.Register("CreateSchema_20180119_110256", m)
}

// Run the migrations
func (m *CreateSchema_20180119_110256) Up() {
	 m.SQL(`
	 	CREATE TABLE IF NOT EXISTS "configurator_configs" (
         "id" SERIAL,
         "type" text NOT NULL DEFAULT '' ,
         "data" text NOT NULL DEFAULT '' ,
         "value" text NOT NULL
     );
	 `);
	 m.SQL(`CREATE INDEX "configurator_configs_type_data" ON "configurator_configs" ("type", "data");`);
	 m.SQL(`CREATE INDEX "configurator_configs_data" ON "configurator_configs" ("data");`);

}

// Reverse the migrations
func (m *CreateSchema_20180119_110256) Down() {
	m.SQL(`DROP TABLE "configurator_configs";`);

}
